# generator-fuse

A generator for [Yeoman](http://yeoman.io).

## What should I know before I get my hands dirty

Ask Justin to configure my computer and teach you about his wizardry in these things before you get started. 

1. [Homebrew](http://brew.sh/)
    * [NodeJS](http://nodejs.org/)
    * [NPM](https://www.npmjs.org/) - Should be installed with NodeJS
    * [GIT](http://git-scm.com/)
    * [Ruby](https://www.ruby-lang.org/en/)
2. [YEOMAN](http://yeoman.io/)
    * [Grunt](http://gruntjs.com/)
    * [Bower](http://bower.io/)

## Getting Started

### What is Yeoman?

Trick question. It's not a thing. It's this guy:

![](http://i.imgur.com/JHaAlBJ.png)

Basically, he wears a top hat, lives in your computer, and waits for you to tell him what kind of application you wish to create.

Not every new computer comes with a Yeoman pre-installed. He lives in the [npm](https://npmjs.org) package repository. You only have to ask for him once, then he packs up and moves into your hard drive. *Make sure you clean up, he likes new and shiny things.*

```
$ npm install -g yo
```

### Yeoman Generators

Yeoman travels light. He didn't pack any generators when he moved in. You can think of a generator like a plug-in. You get to choose what type of application you wish to create, such as a Backbone application or even a Chrome extension.

To install generator-fuse from npm, run:

```
$ npm install -g generator-fuse
```

Finally, initiate the generator:

```
$ yo fuse
```

To uninstall the generator:

```
npm uninstall -g generator-fuse
```
