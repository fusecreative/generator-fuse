'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');


var FuseGenerator = module.exports = function FuseGenerator(args, options, config) {
    yeoman.generators.Base.apply(this, arguments);

    this.on('end', function () {
        this.installDependencies({ skipInstall: options['skip-install'] });
    });

    this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(FuseGenerator, yeoman.generators.Base);

FuseGenerator.prototype.askFor = function askFor() {
    var cb = this.async();

    // have Yeoman greet the user.
    console.log(this.yeoman);
    console.log(chalk.magenta('Out of the box CMSMS v1.11.10 (english), HTML5 Boilerplate, Bootstrap 3 and jQuery are included.'));

    var prompts = [
        {
            name: 'appname',
            message: 'What is the name of your project?',
        },
        {
            type: 'checkbox',
            name: 'features',
            message: 'What more would you like?',
            choices: [{
                name: 'FontAwesome',
                value: 'includeFontAwesome',
                checked: true
            }, {
                name: 'Cycle2 Image Rotator',
                value: 'includeCycle',
                checked: true
            }]
        }
    ];

    this.prompt(prompts, function (answers) {
        var features = answers.features;

        function hasFeature(feat) { return features.indexOf(feat) !== -1; }

        // manually deal with the response, get back and store the results.
        // we change a bit this way of doing to automatically do this in the self.prompt() method.
        this.includeFontAwesome = hasFeature('includeFontAwesome');
        this.includeCycle = hasFeature('includeCycle');

        // `answers` is an object passed in containing the response values, named in
        // accordance with the `name` property from your prompt object. So, for us:
        this.appname = answers.appname;

        cb();
    }.bind(this));
};

FuseGenerator.prototype.cmsms = function cmsms() {
    this.directory('cmsms_latest', 'dev');
};

FuseGenerator.prototype.gruntfile = function gruntfile() {
    this.template('Gruntfile.js');
};

FuseGenerator.prototype.packageJSON = function packageJSON() {
    this.template('_package.json', 'package.json');
};

FuseGenerator.prototype.git = function git() {
    this.copy('gitignore', '.gitignore');
    this.copy('gitattributes', '.gitattributes');
};

FuseGenerator.prototype.bower = function bower() {
    this.copy('bowerrc', '.bowerrc');
    this.template('_bower.json', 'bower.json');
};

FuseGenerator.prototype.htmlhint = function htmlhint() {
    this.copy('htmlhintrc', '.htmlhintrc');
};

FuseGenerator.prototype.jshint = function jshint() {
    this.copy('jshintrc', '.jshintrc');
};

FuseGenerator.prototype.csslint = function csslint() {
    this.copy('csslintrc', '.csslintrc');
};

FuseGenerator.prototype.editorConfig = function editorConfig() {
    this.copy('editorconfig', '.editorconfig');
};

FuseGenerator.prototype.h5bp = function h5bp() {
    this.copy('favicon.ico', 'dev/favicon.ico');
    this.copy('apple-touch-icon.png', 'dev/apple-touch-icon.png');
    this.copy('robots.txt', 'dev/robots.txt');
    this.copy('humans.txt', 'dev/humans.txt');
    this.copy('htaccess', 'dev/.htaccess');
};

FuseGenerator.prototype.app = function app() {
    this.mkdir('dist');
    this.directory('_cmsms', 'dev/_cmsms');
    this.template('index.html', 'dev/_cmsms/Templates/Page_Templates/index.html');
    this.mkdir('dev/_cmsms/_database');
    this.directory('assets', 'dev/assets');
    this.mkdir('dev/assets/css/bootstrap');
};
