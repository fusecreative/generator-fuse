<?php

clearstatcache();

// pass array of items from template into array
$theFile = $params['theFile'];

if (file_exists($theFile)) {
    $result = "true";
} else {
    $result = "false";
}

// make arrays available to smarty
$smarty->assign('result', $result);

?>