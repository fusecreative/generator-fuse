// Generated on <%= (new Date).toISOString().split('T')[0] %> using <%= pkg.name %> <%= pkg.version %>
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        yeoman: {
            // Configurable paths
            app: 'dev',
            dist: 'dist'
        },

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            html: {
                files: ['<%%= yeoman.app %>/*.html', '<%%= yeoman.app %>/_cmsms/{,*/}*.html'],
                tasks: ['newer:htmlhint:all']
            },
            js: {
                files: ['<%%= yeoman.app %>/assets/js/{,*/}*.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            jstest: {
                files: ['test/spec/{,*/}*.js'],
                tasks: ['test:watch']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            styles: {
                files: ['<%%= yeoman.app %>/assets/css/{,*/}*.css'],
                tasks: ['newer:copy:styles', 'autoprefixer', 'csslint']
            },
            compileBootstrap: {
                files: ['<%%= yeoman.app %>/assets/css/bootstrap/less/**/*.less'],
                tasks: ['less']
            },
            livereload: {
                options: {
                    livereload: '<%%= connect.options.livereload %>'
                },
                files: [
                    '<%%= yeoman.app %>/{,*/}*.html',
                    '.tmp/assets/css/{,*/}*.css',
                    '<%%= yeoman.app %>/assets/img/{,*/}*.{gif,jpeg,jpg,png,svg,webp}'
                ]
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                // Change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    open: true,
                    base: [
                        '.tmp',
                        '<%%= yeoman.app %>'
                    ]
                }
            },
            test: {
                options: {
                    port: 9001,
                    base: [
                        '.tmp',
                        'test',
                        '<%%= yeoman.app %>'
                    ]
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%%= yeoman.dist %>',
                    livereload: false
                }
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%%= yeoman.dist %>/*',
                        '!<%%= yeoman.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },

        htmlhint: {
            options: {
                htmlhintrc: '.htmlhintrc'
            },
            all: {
                src: ['<%%= yeoman.app %>/*.html', '<%%= yeoman.app %>/_cmsms/{,*/}*.html']
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%%= yeoman.app %>/assets/js/{,*/}*.js',
                '!<%%= yeoman.app %>/assets/js/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },

        less: {
            compile: {
                options: {
                    paths: ['<%%= yeoman.app %>/assets/css/bootstrap/less'],
                },
                files: {
                    '<%%= yeoman.app %>/assets/css/bootstrap.css': '<%%= yeoman.app %>/assets/css/bootstrap/less/bootstrap.less'
                }
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['> 5%', 'last 2 version', 'ie > 8', 'Firefox > 15', 'Safari > 5', 'Opera > 17']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/assets/css/',
                    src: '{,*/}*.css',
                    dest: '.tmp/assets/css/'
                }]
            }
        },

        csslint: {
            options: {
                csslintrc: '.csslintrc',
            },
            all: {
                files: [{
                    expand: true,
                    cwd: '.tmp/assets/css/',
                    src: ['{,*/}*.css', '!bootstrap.css'],
                    dest: '.tmp/assets/css/'
                }]
            }
        },

        // Renames files for browser caching purposes
        // rev: {
        //     dist: {
        //         files: {
        //             src: [
        //                 '<%%= yeoman.dist %>/assets/js/{,*/}*.js',
        //                 '<%%= yeoman.dist %>/assets/css/{,*/}*.css',
        //                 '<%%= yeoman.dist %>/assets/img/{,*/}*.{gif,jpeg,jpg,png,webp}',
        //                 '<%%= yeoman.dist %>/assets/fonts/{,*/}*.*'
        //             ]
        //         }
        //     }
        // },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        // useminPrepare: {
        //     options: {
        //         dest: '<%%= yeoman.dist %>'
        //     },
        //     html: '<%%= yeoman.app %>/index.html'
        // },

        // Performs rewrites based on rev and the useminPrepare configuration
        // usemin: {
        //     options: {
        //         assetsDirs: ['<%%= yeoman.dist %>']
        //     },
        //     html: ['<%%= yeoman.dist %>/{,*/}*.html'],
        //     css: ['<%%= yeoman.dist %>/assets/css/{,*/}*.css']
        // },


        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%%= yeoman.app %>/assets/img',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%%= yeoman.dist %>/assets/img'
                }]
            }
        },
        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%%= yeoman.app %>/assets/img',
                    src: '{,*/}*.svg',
                    dest: '<%%= yeoman.dist %>/assets/img'
                }]
            }
        },

        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        cssmin: {
            minify: {
                expand: true,
                cwd: '.tmp/assets/css/',
                src: ['{,*/}*.css', '!*.min.css'],
                dest: '<%%= yeoman.dist %>/assets/css/',
                ext: '.min.css'
            }
        },

        uglify: {
            dist: {
                files: {
                    '<%%= yeoman.dist %>/assets/js/scripts.min.js': [
                        '<%%= yeoman.dist %>/assets/js/*.js'
                    ]
                }
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%%= yeoman.app %>',
                    dest: '<%%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt,php}',
                        '.htaccess',
                        'assets/img/{,*/}*.webp',
                        'assets/fonts/{,*/}*.*',
                        'assets/bower_components/bootstrap/dist/fonts/*.*',
                        'admin/**/*.*',
                        'lib/**/*.*',
                        'modules/**/*.*',
                        'plugins/**/*.*',
                        'tmp/**/*.*',
                        'uploads/**/*.*'
                    ]
                }]
            },
            styles: {
                expand: true,
                dot: true,
                cwd: '<%%= yeoman.app %>/assets/css',
                dest: '.tmp/assets/css/',
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up build process
        concurrent: {
            server: [
                'copy:styles'
            ],
            test: [
                'copy:styles'
            ],
            dist: [
                'copy:styles',
                'imagemin',
                'svgmin'
            ]
        }
    });


    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'htmlhint',
            'less',
            'autoprefixer',
            'csslint',
            'connect:livereload',
            'watch'
        ]);
    });

    grunt.registerTask('server', function () {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve']);
    });

    grunt.registerTask('test', function(target) {
        if (target !== 'watch') {
            grunt.task.run([
                'clean:server',
                'concurrent:test',
                'htmlhint',
                'less',
                'autoprefixer',
                'csslint'
            ]);
        }

        grunt.task.run([
            'connect:test',
        ]);
    });

    grunt.registerTask('build', [
        'clean:dist',
        // 'useminPrepare',
        'concurrent:dist',
        'htmlhint',
        'less',
        'autoprefixer',
        'csslint',
        'cssmin',
        'uglify',
        'copy:dist'
        // 'rev',
        // 'usemin',
        // 'htmlmin'
    ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'
    ]);
};
